using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    private InputData Input;
    private CharacterAnimBasedMovement characterMovement;

    public bool onInteractionZone { get; set; }


    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        Input.getInput();
        characterMovement.moveCharacter(Input.hMovement, Input.vMovement, cam, Input.jump, Input.dash);
    }
}
