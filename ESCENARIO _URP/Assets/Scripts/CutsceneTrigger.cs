using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CutsceneTrigger : MonoBehaviour
{
    [SerializeField] PlayableDirector cutscene;
    [SerializeField] GameObject playerGameObj;
    GameObject originalPlayerObj;

    // Start is called before the first frame update
    void Start()
    {
        cutscene.stopped+=OnPlayableDirectorStopped;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.CompareTag("Player"))
        {
            cutscene.Play();
            playerGameObj.SetActive(true);
            originalPlayerObj = other.gameObject;
            originalPlayerObj.SetActive(false);

            this.gameObject.SetActive(false);
        }    
    }

    void OnPlayableDirectorStopped(PlayableDirector aDirector)
    {
        originalPlayerObj.transform.position = playerGameObj.transform.position;
        playerGameObj.SetActive(false);
        originalPlayerObj.SetActive(true);
        cutscene.stopped-=OnPlayableDirectorStopped;
    }
}
